﻿using HZY.EFCore.DbContexts;
using HZY.Models.Entities.Framework;
using HZY.Repositories.BaseRepositories.Impl;

namespace HZY.Repositories.Framework;

public class SysMenuFunctionRepository : AdminEFCoreBaseRepository<SysMenuFunction>
{
    public SysMenuFunctionRepository(AdminBaseDbContext context) : base(context)
    {
    }
}