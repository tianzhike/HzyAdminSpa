﻿using HZY.EFCore.Extensions;
using HZY.EFCore.Models;
using HZY.Infrastructure;
using HZY.Infrastructure.ApiResultManage;
using HZY.Models.DTO;
using HZY.Models.Entities;
using HZY.Models.Entities.Framework;
using HZY.Repositories.Framework;
using HZY.Services.Admin.ServicesAdmin;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZY.Services.Admin.Framework;

/// <summary>
/// 角色 菜单 功能服务
/// </summary>
public class SysRoleMenuFunctionService : AdminBaseService<SysRoleMenuFunctionRepository>
{
    private readonly SysMenuRepository _sysMenuRepository;
    private readonly SysFunctionRepository _sysFunctionRepository;
    private readonly SysMenuFunctionRepository _sysMenuFunctionRepository;

    public SysRoleMenuFunctionService(SysRoleMenuFunctionRepository repository,
        SysMenuRepository sysMenuRepository,
        SysFunctionRepository sysFunctionRepository,
        SysMenuFunctionRepository sysMenuFunctionRepository) : base(repository)
    {
        _sysMenuRepository = sysMenuRepository;
        _sysFunctionRepository = sysFunctionRepository;
        _sysMenuFunctionRepository = sysMenuFunctionRepository;
    }


    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="form"></param>
    /// <returns></returns>
    public async Task<Guid> SaveFormAsync(List<SysRoleMenuFunctionFormDto> form)
    {
        var sysRoleMenuFunctions = new List<SysRoleMenuFunction>();
        var roleId = form.Count > 0 ? form[0].RoleId : default;

        if (roleId == Guid.Empty) return default;

        //开启事务
        using var tran = await this.Repository.Orm.BeginTransactionAsync();

        try
        {
            await this.Repository.DeleteAsync(w => w.RoleId == roleId && form.Select(w => w.MenuId).Contains(w.MenuId));

            foreach (var item in form)
            {
                var menuId = item.MenuId;
                var functionIds = item.FunctionIds;

                var list = functionIds
                    .Select(item => new SysRoleMenuFunction { MenuId = menuId, RoleId = roleId, FunctionId = item })
                    .ToList();

                sysRoleMenuFunctions.AddRange(list);
            }

            await this.Repository.InsertRangeAsync(sysRoleMenuFunctions);

            await tran.CommitAsync();
        }
        catch (Exception)
        {
            await tran.RollbackAsync();
            throw;
        }

        return roleId;
    }

    /// <summary>
    /// 获取 角色 勾选的菜单功能集合 
    /// </summary>
    /// <param name="roleId"></param>
    /// <returns></returns>
    public async Task<List<Dictionary<string, object>>> GetRoleMenuFunctionByRoleIdAsync(Guid roleId)
    {
        var allMenus = await _sysMenuRepository.ToListAllAsync();
        var allMenuFunction = await _sysMenuFunctionRepository.ToListAllAsync();
        var allRoleMenuFunction = await this.Repository.Select.Where(w => w.RoleId == roleId).ToListAsync();
        var allFunctions = await _sysFunctionRepository.ToListAllAsync();

        var result = new List<Dictionary<string, object>>();

        foreach (var item in allMenus)
        {
            //组装菜单
            var menuFunctions = allMenuFunction.Where(w => w.MenuId == item.Id);
            //获取菜单拥有的按钮数组
            var queryFunctions = (from menuFunction in menuFunctions
                                  from function in allFunctions.Where(w => w.Id == menuFunction.FunctionId)
                                  select new { Id = function.Id, Label = function.Name }
                                 ).ToList()
                                 ;
            //获取当前角色勾选该菜单的按钮数组
            var checkedFunctionIds = allRoleMenuFunction
                .Where(w => w.MenuId == item.Id)
                .Select(w => w.FunctionId)
                .ToList()
                ;

            result.Add(new Dictionary<string, object>
            {
                ["id"] = item.Id,
                ["name"] = item.Name,
                ["number"] = item.Number,
                ["parentId"] = item.ParentId,
                ["levelCode"] = item.LevelCode,
                ["menuFunctions"] = queryFunctions,
                ["checkedFunctionIds"] = checkedFunctionIds,
                ["checkAll"] = queryFunctions.Count == checkedFunctionIds.Count,
                ["indeterminate"] = checkedFunctionIds.Count > 0 && checkedFunctionIds.Count < queryFunctions.Count
            });
        }

        return result;
    }

}